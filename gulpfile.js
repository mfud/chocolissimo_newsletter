'use strict';

const gulp = require('gulp');
const mjmlEngine = require('mjml').default;
const mjml = require('gulp-mjml');
const browsersync = require('browser-sync').create();
const panini = require('panini');
const imagemin = require("gulp-imagemin");
const del = require("del");

// Clean assets
function clean() {
    return del(["./dist"]);
}

// panini
function Panini(done){
    gulp.src('src/*.mjml')
        .pipe(panini({
            root: 'src/',
            layouts: 'src/layouts/',
            partials: 'src/partials/',
            helpers: 'src/helpers/',
            data: 'src/data/'
        }))
        .pipe(gulp.dest('src/dist_mjml/'));
    done();
}

// Load updated HTML templates and partials into Panini
function resetPages(done) {
    panini.refresh();
    done();
}

// Clean assets
function cleanImages(done) {
    return del(["./dist/img"]);
    done();
}

// Optimize Images
function images() {
    return gulp
        .src("./src/img/*")
        // .pipe(newer("./_site/assets/img"))
        .pipe(
            imagemin([
                imagemin.gifsicle({ interlaced: true }),
                imagemin.jpegtran({ progressive: true }),
                imagemin.optipng({ optimizationLevel: 7 }),
                imagemin.svgo({
                    plugins: [
                        {
                            removeViewBox: false,
                            collapseGroups: true
                        }
                    ]
                })
            ], {verbose: true})
        )
        .pipe(gulp.dest("./dist/img"));
}

// copyImages
function copyImages(done) {
    gulp.src('./src/img/*')
        .pipe(gulp.dest('./dist/img'));
    done();
}

// mjmlHtml
function mjmlHtml(done){
    gulp.src('src/dist_mjml/*.mjml')
        .pipe(mjml(mjmlEngine, {minify: true}, {beautify: false}))
        .pipe(gulp.dest('./dist'));
    done();
}

// mjmlHtmlBeautify
function mjmlHtmlBeautify(done) {
    gulp.src('src/*.mjml')
        .pipe(mjml(mjmlEngine, {minify: false}, {beautify: true}))
        .pipe(gulp.dest('./dist'));
    done();
}

// BrowserSync
function browserSync(done) {
    browsersync.init({
        server: {
            baseDir: "./dist/"
        },
        port: 3000
    });
    done();
}

// BrowserSync Reload
function browserSyncReload(done) {
    browsersync.reload();
    done();
}

// Watch files
function watchFiles(done) {
    gulp.watch(['./src/*.mjml', './src/data/*.json'], change);
    gulp.watch('./src/dist_mjml/*.mjml', gulp.series(mjmlHtml, browserSyncReload));
    gulp.watch('./src/img/*', copyImages, browserSyncReload);
    done();
}


// Watch files
// function watchFiles() {
//     gulp.watch('./src/*.mjml', (Panini));
//     gulp.watch('./src/dist_mjml/*.mjml', (mjmlHtml));
//     gulp.watch('./src/data/*.json', json);
//     gulp.watch('./src/img/*', copyImages, browserSync);
//     gulp.watch('./dist/*.html', (browserSyncReload));
// }

// define complex tasks
const change = gulp.series(resetPages, Panini, mjmlHtml);
const build = gulp.series(Panini, mjmlHtml, mjmlHtml);
const buildBeautify = gulp.series(Panini, mjmlHtmlBeautify);
const watch = gulp.parallel(watchFiles, browserSync);
const dev = gulp.series(cleanImages, copyImages, Panini, mjmlHtml, watch);


// export tasks

exports.images = images;
exports.panini = Panini;
exports.buildBeautify = buildBeautify;
exports.default = dev;